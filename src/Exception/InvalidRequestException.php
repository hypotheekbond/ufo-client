<?php
declare(strict_types = 1);

namespace Ufo\Client\Exception;

/**
 * Class InvalidRequestException
 */
class InvalidRequestException extends \RuntimeException
{
}
