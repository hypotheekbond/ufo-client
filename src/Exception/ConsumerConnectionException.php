<?php
declare(strict_types = 1);

namespace Ufo\Client\Exception;

/**
 * Class ConsumerConnectionException
 */
class ConsumerConnectionException extends \RuntimeException
{
}
