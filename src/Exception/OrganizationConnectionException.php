<?php
declare(strict_types = 1);

namespace Ufo\Client\Exception;

/**
 * Class OrganizationConnectionException
 */
class OrganizationConnectionException extends \RuntimeException
{
}
