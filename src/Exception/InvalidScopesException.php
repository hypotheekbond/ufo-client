<?php
declare(strict_types = 1);

namespace Ufo\Client\Exception;

/**
 * Class RefreshTokenInvalidException
 */
final class InvalidScopesException extends InvalidRequestException
{
}
